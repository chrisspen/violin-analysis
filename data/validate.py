#!/usr/bin/env python
import os

import yaml

VIOLA = 'viola'
VIOLIN = 'violin'
VIOLIN5STRING = 'violin5string'
GUITAR = 'guitar'

count = 0
for path in sorted(os.listdir('.')):
    count += 1
    if not os.path.isdir(path):
        continue
    print(path)
    parts = path.split(' - ')
    manifest_fn = os.path.join(path, 'manifest.yml')
    manifest_exists = os.path.isfile(manifest_fn)
    if parts[0] in (VIOLA, VIOLIN, VIOLIN5STRING):
        instrument_type, brand_name, model_name, string_brand_name, string_model_name, bow_brand_name, bow_model_name, rosin_brand_name, rosin_type, dt = parts
        #assert manifest_exists
        if not manifest_exists:
            with open(manifest_fn, 'w') as fout:
                yaml.dump(dict(
                    version=1,
                    instrument_type=instrument_type,
                    instrument_brand=brand_name,
                    instrument_model=model_name,
                    string_brand=string_brand_name,
                    string_model=string_model_name,
                    bow_brand=bow_brand_name,
                    bow_model=bow_model_name,
                    rosin_brand=rosin_brand_name,
                    rosin_type=rosin_type,
                    rosin_shape='puck',
                    date_evaluated=dt,
                ), fout)
    elif parts[0] == GUITAR:
        instrument_type, brand_name, model_name, string_brand_name, string_model_name, dt = parts
    else:
        raise NotImplementedError('Unsupported instrument type: %s' % parts[0])
print('%i profiles scanned.' % count)
