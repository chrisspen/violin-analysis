"""
To run a specific run:

    python tests.py Tests.test_foo

"""
import unittest

from analyze import renormalize, float_to_int

class Tests(unittest.TestCase):

    def test_float_to_int(self):
        self.assertEqual(float_to_int(0.9), 1)
        self.assertEqual(float_to_int(0.1), 0)

    def test_renormalize(self):
        old_range = (0, 100)
        new_range = (0, 4)
        data = [
            (10, 0),
            (23, 0),
            (24, 0),
            (25, 1),
            (30, 1),
            (49, 1),
            (50, 2),
            (99, 3),
            (100, 4),
        ]
        for input_value, expected_output in data:
            print('Checking %s' % input_value)
            v = renormalize(input_value, old_range, new_range, typ=int)
            self.assertEqual(v, expected_output)

if __name__ == '__main__':
    unittest.main()
