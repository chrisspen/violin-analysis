#!/usr/bin/env python
"""
Plots FFT for violin scale recordings.

https://en.wikipedia.org/wiki/Timbre

https://en.wikipedia.org/wiki/Fundamental_frequency

http://www.contrabass.com/pages/frequency.html

G3 = 196 Hz

D4 = 293.66 Hz

A4 = 440 Hz

E5 = 659.26 Hz

Musical notes go from 4 Hz to 1046 Hz.

The human ear can register frequences between about 20 Hz to 20,000 Hz.

"""
import os
import pickle
import argparse

import yaml
import numpy as np
from scipy.io import wavfile
import scipy as sp
import matplotlib.pyplot as plt
import librosa

DATA_DIR = './data'

VIOLIN = 'violin'
VIOLA = 'viola'
GUITAR = 'guitar'
VIOLIN5STRING = 'violin5string'

VIOLIN_NOTES = ['G3', 'D4', 'A4', 'E5']
VIOLA_NOTES = ['C3', 'G3', 'D4', 'A4']
GUITAR_NOTES = ['E2', 'A2', 'D3', 'G3', 'B3', 'E4']
VIOLIN5STRING_NOTES = ['C3', 'G3', 'D4', 'A4', 'E5']

INDEXED_VIOLIN_NOTES = [(1, 'G3'), (2, 'D4'), (3, 'A4'), (4, 'E5')]
INDEXED_VIOLA_NOTES = [(1, 'C3'), (1, 'G3'), (2, 'D4'), (3, 'A4')]
INDEXED_GUITAR_NOTES = [(1, 'E2'), (2, 'A2'), (3, 'D3'), (4, 'G3'), (5, 'B3'), (6, 'E4')]
INDEXED_VIOLIN5STRING_NOTES = [(1, 'C3'), (2, 'G3'), (3, 'D4'), (4, 'A4'), (5, 'E5')]

RATE = 8000

FFT_BINS = 100000

plt.style.use('seaborn-dark')
plt.rcParams["figure.figsize"] = (18, 10)

def load_signal_wav(name):
    signal, _ = librosa.load(name, sr = RATE, mono = True)
    return signal

def find_peaks(frequencies, amplitudes, width, lookaround):
    peak_indices = sp.signal.find_peaks_cwt(amplitudes, widths = (width,))
    amplitudes_maxima = list(map(lambda idx: np.max(amplitudes[idx - lookaround:idx + lookaround]), peak_indices))
    frequencies_maxima = frequencies[np.isin(amplitudes, amplitudes_maxima)]
    return frequencies_maxima, amplitudes_maxima

def get_freq_amps(fn, verbose=0):
    """
    Reads an audio wav file and returns a tuple of it's FFT and normalized amplitudes for each frequency.
    """
    if verbose:
        print('Loading filename: %s' % fn)
    sample_rate, not_normalized_signal = wavfile.read(fn)

    if verbose:
        print('Calculating FFT..')
    t = np.arange(not_normalized_signal.shape[0])
    freq = np.fft.fftfreq(t.shape[-1])*sample_rate
    _sp = np.fft.fft(not_normalized_signal)
    amplitudes = abs(_sp.real)

    if verbose:
        print('Normalizing.')
    normed_amplitudes = amplitudes/max(abs(amplitudes))

    return freq, normed_amplitudes

def graph_spectrum(fn, label, offset=0, peaks=False):
    freq, amplitudes = get_freq_amps(fn)

    print('Plotting FFT...')
    plt.plot(freq+offset, amplitudes, label=label)

def graph_spectrum2(fn, label, offset=0, peaks=False):
    print('Loading filename: %s' % fn)
    samples_original = load_signal_wav(fn)

    print('Calculating FFT..')
    N = samples_original.shape[0]
    spectrum = sp.fftpack.fft(samples_original)
    frequencies = sp.fftpack.fftfreq(N, 1 / RATE)[:N//2]
    amplitudes = np.abs(sp.fftpack.fft(samples_original))[:N//2]

    print('Plotting FFT...')
    plt.plot(frequencies+offset, amplitudes, label=label)

    if peaks:
        try:
            print('Calculating peaks...')
            # peak_indices = sp.signal.find_peaks_cwt(amplitudes, widths=(60,))
            frequencies_maxima, amplitudes_maxima = find_peaks(frequencies, amplitudes, 100, 50)

            print('Plotting peaks...')
            # plt.plot(frequencies[peak_indices], amplitudes[peak_indices], 'bx')
            plt.plot(frequencies_maxima+offset, amplitudes_maxima, 'bx')
        except ValueError as exc:
            print('Error calculating peaks: %s' % exc)

def show_fft_note(profiles, note, xlim=None, offset_increment=100, show=False, fn=None, peaks=False):
    if xlim is None:
        xlim = (0, 10000)
    if fn is None:
        fn = f'output/{note}.png'
    plt.clf()

    print('Generating plot...')

    for i, profile in enumerate(profiles):
        graph_spectrum(
            fn=f'data/{profile}/{note}.wav',
            label=profile, peaks=peaks, offset=offset_increment*(i+1))

    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude')
    plt.title(f'FFT - {note}')
    plt.xlim(xlim)
    plt.grid()
    plt.legend(loc='best')

    if fn:
        print('Saving figure to %s...' % fn)
        plt.savefig(fn)
    if show:
        plt.show()

def float_to_int(v):
    """
    Convert a float to an integer.
    """
    return int(round(v))

def renormalize(value, range1, range2, typ=None):
    """
    Scales a value from one range to another.
    """
    delta1 = range1[1] - range1[0]
    delta2 = range2[1] - range2[0]
    new_value = (delta2 * (value - range1[0]) / delta1) + range2[0]
    if typ:
        new_value = typ(new_value)
    return new_value

def get_binner(data, n):
    """
    Returns a function that can digitize the given list of numbers into N separate bins.
    """
    min_value = min(data)
    max_value = max(data)
    return lambda v: min(max(renormalize(v, (min_value, max_value), (0, n-1), typ=int), 0), n-1)

def get_histogram_bins(binner, n, freqs, amps):
    """
    Given the binner and n used in get_binner(), digitizes a key array, freq, and uses it to aggregate a value array, amps.
    Returns the aggregated array.
    """
    bin_indexes = [binner(freq) for freq in freqs]
    new_amps = [0]*n
    for bin_index, amp in zip(bin_indexes, amps):
        assert 0 <= bin_index <= n, 'Invalid bin index: %s' % bin_index
        new_amps[bin_index] += amp
    return new_amps

def get_cached_bins_fn(fn):
    full_filename, ext = os.path.splitext(fn)
    dir_name, part_name = os.path.split(full_filename)
    cache_fn = os.path.join(dir_name, '.'+part_name+'.pkl')
    return cache_fn

def load_cached_bins(fn):
    cache_fn = get_cached_bins_fn(fn)
    if os.path.isfile(cache_fn):
        print('loading:', cache_fn)
        with open(cache_fn, 'rb') as fin:
            return pickle.load(fin)

def save_cached_bins(data, fn):
    cache_fn = get_cached_bins_fn(fn)
    with open(cache_fn, 'wb') as fout:
        pickle.dump(data, fout)

def calculate_error(target, profiles, typ, use_cache=True, only_notes=None):
    """
    Calculates the error difference between the target audio profile and a list of other profiles.
    """
    total_errors = {} # {candidate_index: error_sum}

    assert profiles, 'No profiles specified!'

    if typ == VIOLIN:
        notes = VIOLIN_NOTES
    elif typ == VIOLIN5STRING:
        notes = VIOLIN5STRING_NOTES
    elif typ == VIOLA:
        notes = VIOLA_NOTES
    elif typ == GUITAR:
        notes = GUITAR_NOTES
    else:
        raise NotImplementedError('Type %s is not yet supported.' % typ)

    only_notes = [_ for _ in (only_notes or '').upper().split(',') if _.strip()]
    if only_notes:
        notes = [_ for _ in notes if _ in only_notes]
        assert notes, 'All notes excluded!'

    for note in notes:
        print('-'*80)
        print('Calculating error for note: %s' % note)
        target_fn = f'data/{target}/{note}.wav'
        candidates = []
        for profile in profiles:
            candidates.append(f'data/{profile}/{note}.wav')

        # Calculate binned target FFT.
        target_freq, target_amps = get_freq_amps(target_fn)
        target_binner = get_binner(target_freq, n=FFT_BINS)
        target_amp_bins = load_cached_bins(target_fn) if use_cache else None
        if target_amp_bins is None:
            target_amp_bins = get_histogram_bins(target_binner, n=FFT_BINS, freqs=target_freq, amps=target_amps)
            save_cached_bins(target_amp_bins, target_fn)

        for i, candidate_fn in enumerate(candidates):
            print('candidate_fn:', candidate_fn)

            # Calculate binned candidate FFT.
            candidate_amp_bins = load_cached_bins(candidate_fn) if use_cache else None
            if candidate_amp_bins is None:
                candidate_freq, candidate_amps = get_freq_amps(candidate_fn, verbose=0)
                candidate_amp_bins = get_histogram_bins(target_binner, n=FFT_BINS, freqs=candidate_freq, amps=candidate_amps)
                save_cached_bins(candidate_amp_bins, candidate_fn)

            # Calculate mean squared error.
            difference_array = np.subtract(target_amp_bins, candidate_amp_bins)
            squared_array = np.square(difference_array)
            error = squared_array.mean()
            print('error:', error)

            total_errors.setdefault(i, 0)
            total_errors[i] += error

    print('='*80)
    print('Total Errors:')
    for idx, err_sum in sorted(total_errors.items(), key=lambda o: o[1]):
        name = profiles[idx]
        print('%.09f Profile %i: %s' % (err_sum, idx, name))

def get_profile_dir(pattern, exclude=None, typ=None):
    """
    Finds audio profiles matching one or more patterns.

    pattern := A string or list of strings to check for matches against audio profile directory names. Passing "all" will match all profiles.
    """
    if isinstance(pattern, str):
        patterns = [pattern]
    else:
        assert isinstance(pattern, (tuple, list))
        patterns = pattern
    assert patterns, 'No patterns specified!'
    matches = []
    for f in sorted(os.listdir(DATA_DIR)):
        # print('checking file:', f)
        for _pattern in patterns:
            if exclude and f == exclude:
                continue
            if not os.path.isdir(os.path.join(DATA_DIR, f)):
                continue

            if typ:
                manifest_fn = os.path.join(DATA_DIR, f, 'manifest.yml')
                if os.path.isfile(manifest_fn):
                    with open(manifest_fn) as fin:
                        manifest_data = yaml.load(fin)
                        instrument_types = set(manifest_data['instrument_type'].split('|'))
                        if typ not in instrument_types:
                            print('skipping %s because does not have "%s"' % (f, typ))
                            continue
                else:
                    print('skipping due to lack of manifest')
                    continue

            if _pattern == 'all':
                matches.append(f)
            elif ' - '.join(f.lower().split(' - ')[1:]).startswith(_pattern.lower()):
                # type is first field, followed by vendor name, so ignore type when searching
                matches.append(f)
    return matches

def plot(profiles, show=False):
    assert profiles, 'No profiles given!'
    for idx, note in INDEXED_VIOLIN_NOTES:
        xlim=(0, 6000)
        offset_increment=25
        if note == 'E5':
            xlim=(0, 10000)
            offset_increment=100
        show_fft_note(profiles, note, fn=f'output/{idx}-{note}.png', show=show, xlim=xlim, offset_increment=offset_increment)

def main():

    parser = argparse.ArgumentParser(description='Process some integers.')
    subparsers = parser.add_subparsers(help='sub-command help', dest='action')

    parser_error = subparsers.add_parser('error', help='Calculate error to target profile')
    parser_error.add_argument("type", help='Type of instrument. violin|viola')
    parser_error.add_argument("profiles", nargs='+', help='List of profiles to compare. First is the target.')
    parser_error.add_argument("--notes", default='', help='A comma delimted list of notes to check.')
    parser_error.add_argument("--no-cache", action='store_true', default=False, help='If given, disables use of cache to retrieve data.')

    parser_plot = subparsers.add_parser('plot', help='Plot profile FFTs.')
    parser_plot.add_argument("type", help='Type of instrument. violin|viola')
    parser_plot.add_argument("profiles", nargs='+', help='List of profiles to compare. First is the target.')
    parser_plot.add_argument("--show", default=False, action='store_true', help='If given, displays the plot in a window.')

    args = parser.parse_args()
    print('args:', args)
    if args.action == 'error':
        target_pattern = args.profiles[0]
        target_profiles = get_profile_dir(target_pattern, typ=args.type)
        if not target_profiles:
            raise Exception('No profile found for "%s".' % target_pattern)
        if len(target_profiles) > 1:
            raise Exception('Multiple profiles found for "%s"' % target_pattern)
        target_profile = target_profiles[0]
        print('target_profile:', target_profile)

        other_profiles = get_profile_dir(args.profiles[1:], exclude=target_profile, typ=args.type)
        print('other_profiles:', other_profiles)

        calculate_error(target=target_profile, profiles=other_profiles, typ=args.type, use_cache=not args.no_cache, only_notes=args.notes)

    elif args.action == 'plot':
        profiles = get_profile_dir(args.profiles, typ=args.type)
        print('profiles:', profiles)
        plot(profiles=profiles, show=args.show)


if __name__ == '__main__':
    main()
