numpy==1.20.2
scipy==1.6.2
matplotlib==3.4.1
librosa==0.8.0
PyYAML==5.4.1
