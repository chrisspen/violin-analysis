#!/bin/bash
# Removes all cached binned FFT data calculations.
find ./data -type f -name '*.pkl' -delete
